package Config::Model::Dpkg::Control::Source::StandardVersion ;

use 5.10.1;

use Mouse;
extends 'Config::Model::Value';

use Sort::Versions;
use feature qw/postderef signatures/;
no warnings qw/experimental::postderef experimental::signatures/;

# Debian only module
use Lintian::Data;
use Lintian::Profile;

# Given the initialisation required for Lintian::Data, using compute
# to extract the default value from Lintain is scary: I'd rather avoid
# running this code several times in an eval as done by compute
my $profile = Lintian::Profile->new();
$profile->load('debian');
Lintian::Data->set_vendor($profile);
my $standards = Lintian::Data->new('standards-version/release-dates', qr/\s+/);
my @std_list = $standards->all();

sub _fetch_std {
    goto &_fetch_std_no_check;
}

sub _fetch_std_no_check {
    return $std_list[0];
}

sub compare_with_last_version ($self, $to_check) {
    return versioncmp($to_check, $self->_fetch_std_no_check);
}

__PACKAGE__->meta->make_immutable;

1;

=head1 NAME

Config::Model::Dpkg::Control::Source::StandardVersion - Standard-Version model

=head1 SYNOPSIS

 Internal use for DPkg model

=head1 DESCRIPTION

This class is derived from L<Config::Model::Value>. Its purpose is to
provide a default value for C<Standard-Version> parameter using Lintian library.

=head1 METHODS

=head2 compare_with_last_version

Compare passed version with the latest version of
C<Standards-Version>. This last version is retrieved from C<lintian>
package. The comparison is done with L<Sort::Versions>.

This methods return -1, 0 or 1 depending if the passed version is
older, equal or newer than latest version of C<Standards-Version>

Example:

 $self->compare_with_last_version('3.9.1'); # returns -1

=head1 AUTHOR

Dominique Dumont, ddumont [AT] cpan [DOT] org

=head1 SEE ALSO

L<Config::Model>,
L<Config::Model::Value>,
