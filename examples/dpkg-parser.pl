package MyParser ;

use strict;
use warnings;

use 5.20.1;

use Log::Log4perl qw(:easy);
Log::Log4perl->easy_init($WARN);

# load role
use Mouse ;
with 'Config::Model::Backend::DpkgSyntax';

package main ;
use IO::File;
use Data::Dumper;

my $file = 'examples/dpkg-test';
my $fh = IO::File->new();
$fh->open("<  $file");

my $parser = MyParser->new() ;

my $data = $parser->parse_dpkg_file($file, $fh, 'yes', 1);
$fh->close;

print Dumper $data;

