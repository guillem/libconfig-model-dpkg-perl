New examples are created in 2 steps.

First create the .in file

Say you want to create a test from package `foo`.

set these variables:

    # create .in file
    # in libconfig-model-dpkg-perl repo
    export TEST=foo_test
    export repo_dir=$PWD
    cd path_to_foo
    licensecheck --encoding utf8 --copyright --machine --deb-fmt --recursive . > $repo_dir/t/scanner/examples/$TEST.in

    # create .out file
    cd - # back to libconfig-model-dpkg-perl repo
    perl -Ilib bin/scan-copyrights t/scanner/examples/$TEST.in > t/scanner/examples/$TEST.out

The .out files may need to be regenated if lib/Dpkg/Copyright/Scanner.pm is changed.
Use a similar command to do so:

    export TEST=bar
    perl -Ilib bin/scan-copyrights t/scanner/examples/$TEST.in > t/scanner/examples/$TEST.out

Be sure to check that the updated output makes sense

